package id.its.ekosetiyo.movieApiCatalogue.api;


import id.its.ekosetiyo.movieApiCatalogue.model.MovieResponse;
import id.its.ekosetiyo.movieApiCatalogue.model.TvResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieClient {

    @GET("discover/movie")
    Call<MovieResponse> MovieList(@Query("api_key") String apiKey);

    @GET("discover/tv")
    Call<TvResponse> TvList(@Query("api_key") String apiKey);
    

}
