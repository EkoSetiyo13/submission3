package id.its.ekosetiyo.movieApiCatalogue;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import id.its.ekosetiyo.movieApiCatalogue.adapter.MovieAdapterFragment;
import id.its.ekosetiyo.movieApiCatalogue.adapter.TvAdapterFragment;
import id.its.ekosetiyo.movieApiCatalogue.api.ApiClient;
import id.its.ekosetiyo.movieApiCatalogue.api.MovieClient;
import id.its.ekosetiyo.movieApiCatalogue.model.Movie;
import id.its.ekosetiyo.movieApiCatalogue.model.MovieResponse;
import id.its.ekosetiyo.movieApiCatalogue.model.Tv;
import id.its.ekosetiyo.movieApiCatalogue.model.TvResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTv extends Fragment {
    private final static String TAG = "MainActivity";
    private final static String API_KEY = "007c868395e80dc2e4a833416b24efa5";
    private RecyclerView.Adapter mAdapter;
    private RecyclerView mrecyclerView;
    Button btnCari;
    private EditText editText;

    public FragmentTv() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv, container, false);


        mrecyclerView = view.findViewById(R.id.recyclerTv);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mrecyclerView.setHasFixedSize(true);
        mrecyclerView.setLayoutManager(layoutManager);



        getAllMovie();

        return view;


    }

    private void getAllMovie() {
        MovieClient apiService = ApiClient.getRetrofit().create(MovieClient.class);
        Call<TvResponse> call = apiService.TvList(API_KEY);

        // panggil progressdialog biar enak kayak nunggu jodoh
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMax(50);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();


        call.enqueue(new Callback<TvResponse>() {
            @Override
            public void onResponse(Call<TvResponse> call, Response<TvResponse> response) {
                progressDialog.dismiss();

                List<Tv> TvList = response.body().getMovies();
                Log.d(TAG, "Jumlah data Movie: " + String.valueOf(TvList.size()));
                //lempar data ke adapter
                mAdapter = new TvAdapterFragment(TvList);
                mrecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onFailure(Call<TvResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, t.toString());
                Toast.makeText(getActivity(), "koneksi error :(", Toast.LENGTH_SHORT).show();

            }
        });
    }





    public void onClick(View v) {

    }
}
