package id.its.ekosetiyo.movieApiCatalogue;

import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView buttonNav;
    FrameLayout mFrame;
    private FragmentMovie fragmentMovie;
    private FragmentTv fragmentTv;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mFrame = findViewById(R.id.frame_layout);
        buttonNav = findViewById(R.id.navigation);

        fragmentTv = new FragmentTv();
        setFragment(fragmentTv);
        fragmentMovie = new FragmentMovie();
        setFragment(fragmentMovie);
        getSupportActionBar().setTitle("Movie");

        ;

        buttonNav.setOnNavigationItemSelectedListener(this);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_tv:
                setFragment(fragmentTv);
                getSupportActionBar().setTitle("Tv show");
                item.setChecked(true);

                return true;
            case R.id.nav_home:
                setFragment(fragmentMovie);
                getSupportActionBar().setTitle("Movie");
                item.setChecked(true);
                return true;

            default:
                return false;

        }
    }

    private void setFragment(FragmentMovie fragmentMovie) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragmentMovie);
        fragmentTransaction.commit();

    }

    private void setFragment(FragmentTv fragmentTv) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragmentTv);
        fragmentTransaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings){
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}